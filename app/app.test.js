import request from 'supertest';
import app from './app';

test('should allow GET request and get a response back with message', () => {
  return request(app).get('/').then((response) => { 
      expect(response.body.message).not.toBe(undefined);
    });
});

test('should allow POST request with data and get a response back with message', () => {
  return request(app).post('/').send({test: 'test message'}).then((response) => {
      expect(response.body.message).not.toBe(undefined);
    });
});