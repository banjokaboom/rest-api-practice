import app from './app';

app.listen(port, function() { //this is what starts up the server and tells it what port to listen to in order to direct traffic to the above routes
    console.log('Listening on ' + port + '!');
});