const express = require('express'); //the package to simplify a back end server using node.js
const cors = require('cors'); //helps prevent cross-origin scripting restrictions, particularly useful when doing tests using localhost
var bodyParser = require('body-parser'); //needed to parse JSON

const app = express(); //instantiating the app as an express app
const port = 3000;

app.use(bodyParser.json()); // for parsing application/json
app.use(cors()); //see above about CORS

app.get('/', function (req, res) { //acts as the default route for the GET method when something hits localhost:3000/
    res.send({
        message: 'GET request successfully received!'
    });
});

app.post('/', function (req, res) { //acts as the default route for the POST method when something hits localhost:3000/
    console.log(req.body); //req.body has the data that was passed as part of the request. For example, if the data was {status: 'alive'}, you can do req.body.status and it will give you the value 'alive'
    res.send({
        message: 'POST request successfully received!'
    });
});

export default app;