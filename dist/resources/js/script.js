//Constant declarations
const $ = jQuery;
const bkData = {
    name: 'Banjo Kaboom',
    job: 'explosive hillbilly musician'
};
const aaData = {
    name: 'Angry Ancestor',
    job: 'disgruntled past relative'
};

/* 
    API constant - the API can be used to search for a single user by adding a number to the end,
    or to create a user by calling via POST and sending data
*/
const USER_API = 'https://reqres.in/api/users/';
const LOCALHOST_NODE_SERVER = 'http://localhost:3000/';

/* 
    Uses fetch api to make an AJAX request to an endpoint
    Pass the URL, method ('GET' or 'POST'), and data is optional
    Data is only required for a POST

    Even though fetch requires a little more work here, it is becoming a standard to use in browsers,
    which means that we don't always have to rely on jQuery to run our AJAX
    Another nice thing is that fetch relies on promises, i.e. fetch().then().catch()
    There are other more technical benefits to fetch that you can read up on the internet
*/
function getFetchData(url, method, data) {
    $('#response').html(''); //wipe out the response content before starting

    fetch(url, {
        method: method,
        headers: { // Headers are very important. There are a variety of settings you can add here, but Accept and Content-Type are two good ones
            Accept: 'application/json', // This tells the endpoint what type of data you are willing to accept
            'Content-Type': 'application/json' // This tells the endpoint what type of data you are sending
        },
        body: JSON.stringify(data) //convert the JSON data passed to a string that can be sent to the API
    }).then(function (response) {
        if (response.ok) { //verify that the response status is 'ok' which equals a status of 200
            /* 
                Because fetch returns a Promise object, we access the JSON in the response by calling then()
                From there, the JSON itself is a Promise object, so we need to call then() right after calling json() to get it
            */
            response.json().then(function (json) {
                printResponseJSON(json);
                if (json.name && json.job && json.id && json.createdAt) { //user was created

                    //reading the JSON data from the response to print it to the screen
                    $('table#created-users tbody').prepend('<tr></tr>');
                    $('table#created-users tbody tr:first-child').append('<td>' + json.name + '</td>');
                    $('table#created-users tbody tr:first-child').append('<td>' + json.job + '</td>');
                    $('table#created-users tbody tr:first-child').append('<td>' + json.id + '</td>');

                    //just me being fancy to format that crappy date we get back from the API to something more readable
                    var createdDate = new Date(json.createdAt);
                    var dateText = (createdDate.getMonth() + 1) + '/' + createdDate.getDate() + '/' + createdDate.getFullYear();
                    var timeText = ((createdDate.getHours() > 12) ? createdDate.getHours() - 12 : createdDate.getHours()) + ':' + (createdDate.getMinutes() < 10 ? '0' + createdDate.getMinutes() : createdDate.getMinutes()) + ((createdDate.getHours() >= 12) ? 'p.m.' : 'a.m.');
                    $('table#created-users tbody tr:first-child').append('<td>' + timeText + ' ' + dateText + '</td>');
                }
            });
        } else { //if the response was not OK, print out the response status and the message relating to it
            console.error('fetch status code: ' + response.status + ', status message: ' + response.statusText);
        }
    }).catch(function (error) { //always have a catch in case anything goes wrong
        console.error(error);
    });
}

/* 
    Uses jQuery AJAX to make an AJAX request to an endpoint.
    Pass the URL, method ('GET' or 'POST'), and data is optional.
    Data is only required for a POST.

    As you'll see here, jQuery is actually easier to set up
    The main difference is that jQuery doesn't use promises, but callback hooks like 'success' and 'error'
*/
function getJQueryAJAXData(url, method, data) {
    $('#response').html(''); //wipe out the response content before starting

    $.ajax({
        url: url,
        type: method,
        dataType: "json",
        headers: { // Headers are very important. There are a variety of settings you can add here, but Accept and Content-Type are two good ones
            Accept: 'application/json', // This tells the endpoint what type of data you are willing to accept
            'Content-Type': 'application/json' // This tells the endpoint what type of data you are sending
        },
        data: JSON.stringify(data), //convert the JSON data passed to a string that can be sent to the API
        success: function (response) {
            /*
                In the case of jQuery, the response is the JSON object already, so we can start breaking it down immediately
            */
            printResponseJSON(response);
            if (response.name && response.job && response.id && response.createdAt) { //user was created

                //reading the JSON data from the response to print it to the screen
                $('table#created-users tbody').prepend('<tr></tr>');
                $('table#created-users tbody tr:first-child').append('<td>' + response.name + '</td>');
                $('table#created-users tbody tr:first-child').append('<td>' + response.job + '</td>');
                $('table#created-users tbody tr:first-child').append('<td>' + response.id + '</td>');

                //just me being fancy to format that crappy date we get back from the API to something more readable
                var createdDate = new Date(response.createdAt);
                var dateText = (createdDate.getMonth() + 1) + '/' + createdDate.getDate() + '/' + createdDate.getFullYear();
                var timeText = ((createdDate.getHours() > 12) ? createdDate.getHours() - 12 : createdDate.getHours()) + ':' + (createdDate.getMinutes() < 10 ? '0' + createdDate.getMinutes() : createdDate.getMinutes()) + ((createdDate.getHours() >= 12) ? 'p.m.' : 'a.m.');
                $('table#created-users tbody tr:first-child').append('<td>' + timeText + ' ' + dateText + '</td>');
            }

        },
        error: function (error) { //always have a catch in case anything goes wrong
            console.error(error);
        }
    });
}

/*
    Nothing fancy here, just helper functions to be used by the frontend
*/

function getSingleUserFetch() {
    getFetchData(USER_API + '2', 'GET');
}

function getSingleUserJQuery() {
    getJQueryAJAXData(USER_API + '2', 'GET');
}

function postCreateUserFetch() {
    getFetchData(USER_API, 'POST', aaData);
}

function postCreateUserJQuery() {
    getJQueryAJAXData(USER_API, 'POST', bkData);
}

/**
 * To demonstrate ability to create and manipulate a JSON object
 * 
 * This example creates an empty JSON object and adds new attribute values after instantiation.
 */
function postCreateUserFormDataFetch() {
    $('#response').html(''); //wipe out the response content before starting

    let data = {}; // Create empty JSON object

    data.name = $('#name').val(); // Add attribute name with value from form
    data.job = $('#job').val(); // Add attribute job with value from form

    if (data.name && data.job) { // Check to make sure that values were passed in before calling the API
        getFetchData(USER_API, 'POST', data);
    } else {
        alert('Required data missing!');
    }
}

/**
 * To demonstrate ability to create and manipulate a JSON object
 * 
 * This example creates a JSON object using the values from the form at the time of instantiation.
 */
function postCreateUserFormDataJQuery() {
    $('#response').html(''); //wipe out the response content before starting

    let data = {
        name: $('#name').val(),
        job: $('#job').val()
    }; // Create JSON object

    if (data.name && data.job) { // Check to make sure that values were passed in before calling the API
        getJQueryAJAXData(USER_API, 'POST', data);
    } else {
        alert('Required data missing!');
    }
}

function getNodeResponse() {
    getJQueryAJAXData(LOCALHOST_NODE_SERVER, 'GET');
}

function postNodeResponse() {
    getJQueryAJAXData(LOCALHOST_NODE_SERVER, 'POST', {
        status: 'alive',
        message: 'this test worked'
    });
}

// Print the response out to the page
function printResponseJSON(json) {
    $('#response').html(JSON.stringify(json)); //since we are being passed a JSON object, it needs to be converted to a string to print it
}

/*
  simple test helper to log test results to the console
  testName is a string to print out in the console
  condition is the result of an expression, i.e. 1 !== 2 is false, 2 === 2 is true
*/
function executeTest(testName, condition) {
    if (condition === true) {
        console.log(testName + ' Passed!');
    } else {
        console.error(testName + ' Failed!');
    }
}

/*
    Run these tests on page load
*/
$(document).ready(function () {
    var testPrintData = {
        test: 'test'
    }; //test data to test the print helper function

    console.log('Executing tests...');

    printResponseJSON(testPrintData);
    executeTest('Print JSON', $('#response').html() === JSON.stringify(testPrintData)); //see if the html printed out matches the JSON object stringified
    $('#response').html('');

    fetch(USER_API, {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }).then(function (response) {
        executeTest('Fetch GET API', response.ok);
    }).catch(function (error) {
        executeTest('Fetch GET API', false);
    });

    fetch(USER_API + 2, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(aaData)
    }).then(function (response) {
        response.json().then(function (json) {
            executeTest('Fetch POST API', json.name === aaData.name);
        })
    }).catch(function (error) {
        executeTest('Fetch POST API', false);
    });

    $.ajax({
        url: USER_API,
        type: 'GET',
        dataType: "json",
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        success: function () {
            executeTest('jQuery AJAX GET API', true);
        },
        error: function () {
            executeTest('jQuery AJAX GET API', false);
        }
    });

    $.ajax({
        url: USER_API + 2,
        type: 'POST',
        dataType: "json",
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        data: JSON.stringify(bkData),
        success: function (response) {
            executeTest('jQuery AJAX POST API', response.name === bkData.name);
        },
        error: function () {
            executeTest('jQuery AJAX POST API', false);
        }
    });
})