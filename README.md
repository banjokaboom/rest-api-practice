## Rest API Practice

Tools/Technology used:

1. VS Code with debugger to run the Node Server
2. Fetch API with polyfills
3. jQuery for AJAX request
4. Promises
5. Simple unit tests
6. Manipulating, parsing and sending JSON
7. Rest API (using https://reqres.in/ as well as a locally-hosted node server)
8. Node server with express (for an endpoint to test)

For the simple app, just navigate to the dist folder and open index.html. App should run fine in a browser.

For the Node server:

- Install node.js: https://nodejs.org/en/download/
- Using a terminal (I like cmder: http://cmder.net/, but you can use the normal windows one), cd into the app directory
- run `npm install`, it will install everything based on the package.json
- after that, launch VS Code, open `app/server.js` and use VS Code to "debug" (F5 shortcut)
- The server is running and you can now hit it from the front end!
- Uncomment the two buttons in `dist/index.html` to be able to see the api calls in action
- If you want to be sure the server is running, you can actually navigate to http://localhost:3000/ in your browser and it will be the same as making a GET request. You should see the success message too!

For running the Jest unit tests for the node server:

- cd into the app directory
- run `npm test`
- That's it!


Extra notes:

- I use `dist/` and `app/` for the project structure because typically Node.js apps are compiled and deployed to `dist/`. All static files are in `dist/` and the app code, in this case the node server code, is in `app/`.
- There is a lot of documentation out there on all of these tools. Here are some links:
	- https://expressjs.com/en/starter/hello-world.html
	- http://api.jquery.com/jquery.ajax/
	- https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
	- https://github.com/github/fetch - for the fetch polyfill (fetch.umd.js)
	- https://jestjs.io/docs/en/using-matchers - Jest, using matchers to evaluate results
	- http://www.albertgao.xyz/2017/05/24/how-to-test-expressjs-with-jest-and-supertest/ - Using Jest with Express



Enjoy!

--------------------------------------------------------------------------
Since this is public, if anyone ever finds this, this was an example/POC project to help out a friend. Coding standards and project structure are my own.